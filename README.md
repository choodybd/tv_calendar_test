### Configuration

You can find configuration files in:

```
src/
  config/
    Api.js
    Config.js
```

Api.js - endpoints for used API

Config.js - additional configuration:

```
1. CALENDAR_WIDTH - width of timeline elements in main view
2. CALENDAR_START_DATE - start date of calendar in main view
3. CALENDAR_END_DATE - end date of calendar in main view
4. THIS_SHOULD_BE_DATE_NOW - on production API this should be set to current day
```

### Running application

1. `npm install` / `yarn install`
2. `npm start` / `yarn start`

This will run development build.

### Building application

1. `npm run build` / `yarn build`
2. `yarn global add serve`
2. `serve -s build`