import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';

const Button = props => {
  return (
    <button styleName={`button${props.addClass}`} onClick={props.clickEvent}>
      <i styleName={`fa fa-${props.icon}`} aria-hidden />
    </button>
  );
};

Button.propTypes = {
  icon: PropTypes.string.isRequired,
  addClass: PropTypes.string,
  clickEvent: PropTypes.func,
};

Button.defaultProps = {
  addClass: '',
  clickEvent: null,
};

export default CSSModules(Button, styles, { allowMultiple: true });
