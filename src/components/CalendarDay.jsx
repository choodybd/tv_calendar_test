import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';

const CalendarDay = props => {
  return (
    <div
      role="button"
      tabIndex={0}
      styleName={`calendarDay${props.activeDate}`}
      data-date={props.date}
      onClick={props.onClick}
    >
      <div>{props.dayName}</div>{props.day}.{props.month}
    </div>
  );
};

CalendarDay.propTypes = {
  dayName: PropTypes.string.isRequired,
  day: PropTypes.number.isRequired,
  date: PropTypes.string.isRequired,
  month: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  activeDate: PropTypes.string,
};

CalendarDay.defaultProps = {
  activeDate: '',
};

export default CSSModules(CalendarDay, styles, { allowMultiple: true });
