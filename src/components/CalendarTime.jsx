import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';
import { CALENDAR_WIDTH } from '../config/Config';

const CalendarTime = props => {
  return (
    <div styleName="calendarTime" style={{ width: CALENDAR_WIDTH * 60 }}>
      {props.time}
    </div>
  );
};

CalendarTime.propTypes = {
  time: PropTypes.string.isRequired,
};

export default CSSModules(CalendarTime, styles, { allowMultiple: true });
