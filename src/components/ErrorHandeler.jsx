import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';

const ErrorHandler = props => {
  return (
    <div styleName="errorHandler">Error: <span>{props.message}</span></div>
  );
};

ErrorHandler.propTypes = {
  message: PropTypes.string.isRequired,
};

export default CSSModules(ErrorHandler, styles, { allowMultiple: true });
