import React from 'react';
import PropTypes from 'prop-types';

const Image = props => {
  return (
    <img src={props.src} alt={props.alt} />
  );
};

Image.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
};

export default Image;
