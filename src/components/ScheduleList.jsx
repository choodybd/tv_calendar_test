import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';

const ScheduleList = props => {
  if (props.list) {
    return (
      <ul styleName={props.name} className={props.c}>
        {props.data}
      </ul>
    );
  }
  return (
    <div styleName={props.name} className={props.c}>
      {props.data}
    </div>
  );
};

ScheduleList.propTypes = {
  data: PropTypes.array.isRequired,
  c: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  list: PropTypes.bool,
};

ScheduleList.defaultProps = {
  list: false,
};

export default CSSModules(ScheduleList, styles, { allowMultiple: true });
