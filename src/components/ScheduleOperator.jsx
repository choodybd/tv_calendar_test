import React from 'react';
import PropTypes from 'prop-types';
import Image from './Image';

const ScheduleOperator = props => {
  if (props.image) {
    return (
      <li>
        <Image src={props.image} alt={props.title} />
      </li>
    );
  }
  return (
    <li>
      {props.title}
    </li>
  );
};

ScheduleOperator.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string.isRequired,
};

ScheduleOperator.defaultProps = {
  image: null,
};

export default ScheduleOperator;
