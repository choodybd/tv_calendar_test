import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';

const SearchForm = props => {
  const activeClass = props.activeClass ? ' active' : '';
  return (
    <div styleName={`searchform${activeClass}`}>
      <input
        className="s-in"
        styleName="searchInput"
        type="text"
        value={props.searchTerm}
        onChange={props.changeSearchTerm}
        placeholder="Search for channel"
      />
    </div>
  );
};

SearchForm.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  changeSearchTerm: PropTypes.func.isRequired,
  activeClass: PropTypes.bool.isRequired,
};

export default CSSModules(SearchForm, styles, { allowMultiple: true });
