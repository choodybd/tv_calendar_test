import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';

const SeasonButton = props => {
  return (
    <button styleName={`seasonButton${props.addClass}`} data-season={props.season} onClick={props.clickEvent}>
      {props.title}
    </button>
  );
};

SeasonButton.propTypes = {
  title: PropTypes.string.isRequired,
  clickEvent: PropTypes.func.isRequired,
  season: PropTypes.number.isRequired,
  addClass: PropTypes.string,
};

SeasonButton.defaultProps = {
  addClass: '',
};

export default CSSModules(SeasonButton, styles, { allowMultiple: true });
