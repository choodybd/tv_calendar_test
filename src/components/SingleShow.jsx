import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import CSSModules from 'react-css-modules';
import styles from '../css/style.css';

const SingleShow = props => {
  return (
    <li styleName="single-show" style={{ width: props.length }}>
      <Link to={`/program/${props.showId}`}>
        <div styleName="title">
          {props.title}
        </div>
        <div styleName="time">
          {props.start} - {props.end}
        </div>
      </Link>
    </li>
  );
};

SingleShow.propTypes = {
  title: PropTypes.string.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  length: PropTypes.number.isRequired,
  showId: PropTypes.string.isRequired,
};

export default CSSModules(SingleShow, styles, { allowMultiple: true });
