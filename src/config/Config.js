export const CALENDAR_WIDTH = 6;
export const CALENDAR_START_DATE = '2017-03-01';
export const CALENDAR_END_DATE = '2017-03-31';
export const THIS_SHOULD_BE_DATE_NOW = '2017-03-18';
