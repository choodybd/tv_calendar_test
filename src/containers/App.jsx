import React, { Component } from 'react';
import Header from './Header';
import Calendar from '../containers/Calendar';
import logo from '../assets/logo.png';
import { CALENDAR_START_DATE, CALENDAR_END_DATE } from '../config/Config';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
    };
  }

  changeSearchTerm = e => {
    this.setState({
      searchTerm: e.target.value.substr(0, 20),
    });
  };

  render() {
    return (
      <div className="app">
        <Header
          logo={logo}
          changeSearchTerm={this.changeSearchTerm}
          searchTerm={this.state.searchTerm}
          addClass="calendarPage"
        />
        <Calendar
          searchTerm={this.state.searchTerm}
          startDate={CALENDAR_START_DATE}
          endDate={CALENDAR_END_DATE}
        />
      </div>
    );
  }
}

export default App;
