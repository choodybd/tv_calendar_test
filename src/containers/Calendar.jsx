import React, { Component } from 'react';
import DragScroll from 'react-dragscroll';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import CalendarDay from '../components/CalendarDay';
import CalendarTime from '../components/CalendarTime';
import ProgramList from './ProgramList';
import Button from '../components/Button';
import styles from '../css/style.css';
import { CALENDAR_WIDTH, THIS_SHOULD_BE_DATE_NOW } from '../config/Config';

class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: this.props.startDate,
      endDate: this.props.endDate,
      days: [],
      time: [],
      loaded: false,
      currentDate: THIS_SHOULD_BE_DATE_NOW,
    };
  }

  componentDidMount() {
    this.getDates();
    this.getTimes();
    this.checkScrolling();
    setTimeout(() => {
      this.setActiveDate();
      this.setActiveTime();
      this.setActiveTimeMarker();
      this.setState({
        loaded: true,
      });
    }, 500);
    setInterval(this.setActiveTimeMarker, 1000 * 10);
  }

  setActiveDate = () => {
    document.querySelector('.cal').scrollLeft =
      document.querySelector(`[data-date="${this.state.currentDate}"]`).offsetLeft -
      window.innerWidth / 2 +
      document.querySelector(`[data-date="${this.state.currentDate}"]`).offsetWidth / 2;
  };

  setActiveTime = () => {
    const minutes = this.checkTime();
    document.querySelector('.tim').scrollLeft =
      minutes * CALENDAR_WIDTH - window.innerWidth / 2;
    if (document.querySelector('.o-lis')) {
      document.querySelector(
        '.o-lis',
      ).style.left = `${document.querySelector('.tim').scrollLeft}px`;
    }
    if (this.state.currentDate !== THIS_SHOULD_BE_DATE_NOW) {
      const currentDate = THIS_SHOULD_BE_DATE_NOW;
      this.setState({
        currentDate: currentDate,
      });
      document.querySelector('.cal').scrollLeft =
        document.querySelector(`[data-date="${currentDate}"]`).offsetLeft -
        window.innerWidth / 2 +
        document.querySelector(`[data-date="${currentDate}"]`).offsetWidth / 2;
    }
  };

  setActiveTimeMarker = () => {
    const minutes = this.checkTime();
    if (document.querySelector('.ctm')) {
      document.querySelector('.ctm').style.left = `${minutes *
      CALENDAR_WIDTH}px`;
    }
  };

  getDates = () => {
    const dateArray = [];
    const endDate = new Date(this.state.endDate);
    const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    let currentDate = new Date(this.state.startDate);
    let addDayToDate;
    while (currentDate < endDate) {
      const fullDay = currentDate.getDate() < 10 ? `0${currentDate.getDate()}` : currentDate.getDate();
      const fullMonth = currentDate.getMonth() + 1 < 10 ? `0${currentDate.getMonth() + 1}` : currentDate.getMonth() + 1;
      const fullDate = `${currentDate.getFullYear()}-${fullMonth}-${fullDay}`;
      const dateName = {
        dayName: dayNames[currentDate.getDay()],
        date: fullDate,
        day: currentDate.getDate(),
        month: currentDate.getMonth() + 1,
      };
      dateArray.push(dateName);
      addDayToDate = currentDate.setDate(currentDate.getDate() + 1);
      currentDate = new Date(addDayToDate);
    }
    this.setState({
      days: dateArray,
    });
  };

  getTimes = () => {
    const timeArray = [];
    for (let i = 0; i < 24; i += 1) {
      const time = `${i}:00`;
      timeArray.push(time);
    }
    this.setState({
      time: timeArray,
    });
  };

  checkTime = () => {
    const activeTime = new Date().toLocaleTimeString();
    const splitTime = activeTime.split(':');
    const minutes = Number(splitTime[0]) * 60 + Number(splitTime[1]);
    return minutes;
  };

  changeDate = (e) => {
    this.setState({
      currentDate: e.target.dataset.date,
    });
    document.querySelector('.cal').scrollLeft =
      document.querySelector(`[data-date="${e.target.dataset.date}"]`).offsetLeft -
      window.innerWidth / 2 +
      document.querySelector(`[data-date="${e.target.dataset.date}"]`).offsetWidth / 2;
  };

  checkScrolling = () => {
    const element = document.querySelector('.cal');
    element.addEventListener('mousedown', () => {
      element.onmousemove = () => {
        element.classList.add('cd-ev');
      };
    });
    element.addEventListener('mouseup', () => {
      element.onmousemove = null;
      element.classList.remove('cd-ev');
    });
  };

  render() {
    const daysList = (value, index) => {
      let activeDate = '';
      if (value.date === this.state.currentDate) {
        activeDate = ' active';
      }
      return (
        <CalendarDay
          key={index}
          day={value.day}
          dayName={value.dayName}
          date={value.date}
          month={value.month}
          activeDate={activeDate}
          onClick={this.changeDate}
        />
      );
    };
    const timeList = (day, index) => {
      return <CalendarTime key={index} time={day} />;
    };
    const loaded = this.state.loaded ? ' loaded' : '';
    return (
      <div styleName={`main${loaded}`}>
        <Button
          addClass=" setActiveTimeButton"
          icon="clock-o"
          clickEvent={this.setActiveTime}
        />
        <DragScroll
          className="cal"
          styleName="calendar"
        ><div>
            {Object.values(this.state.days).map(daysList)}
          </div>
        </DragScroll>
        <DragScroll className="tim" styleName="timeline">
          <div styleName="timelineTimes">
            <div styleName="calendarTimeContainer">
              {this.state.time.map(timeList)}
            </div>
            <ProgramList
              currentDate={this.state.currentDate}
              searchTerm={this.props.searchTerm}
            />
          </div>
        </DragScroll>
      </div>
    );
  }
}

Calendar.propTypes = {
  startDate: PropTypes.string.isRequired,
  endDate: PropTypes.string.isRequired,
  searchTerm: PropTypes.string,
};

Calendar.defaultProps = {
  searchTerm: null,
};

export default CSSModules(Calendar, styles, { allowMultiple: true });
