import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import Button from '../components/Button';
import SearchForm from '../components/SearchForm';
import Image from '../components/Image';
import styles from '../css/style.css';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchActive: false,
    };
  }

  setSearchActive = () => {
    if (this.state.searchActive) {
      this.setState({
        searchActive: false,
      });
    } else {
      this.setState({
        searchActive: true,
      });
      setTimeout(() => {
        document.querySelector('.s-in').focus();
      }, 500);
    }
  };

  render() {
    const logo = this.props.logo
      ? <Image src={this.props.logo} alt="Norigin Media" />
      : 'Norigin Media';
    if (this.props.addClass === 'showPage') {
      return (
        <header styleName={`header ${this.props.addClass}`}>
          <div styleName="col col-4 flex-left">
            <NavLink exact to="/">
              <Button addClass=" black" icon="chevron-left" />
            </NavLink>
          </div>
          <div styleName="col col-4 flex-center">
            {logo}
          </div>
          <div styleName="col col-4 flex-right" />
        </header>
      );
    }

    return (
      <header styleName={`header ${this.props.addClass}`}>
        <div styleName="col col-4 flex-left" />
        <div styleName="col col-4 flex-center">
          {logo}
        </div>
        <div styleName="col col-4 flex-right">
          <Button
            addClass=" black"
            icon="search"
            clickEvent={this.setSearchActive}
          />
        </div>
        <SearchForm
          changeSearchTerm={this.props.changeSearchTerm}
          searchTerm={this.props.searchTerm}
          activeClass={this.state.searchActive}
        />
      </header>
    );
  }
}

Header.propTypes = {
  searchTerm: PropTypes.string,
  logo: PropTypes.string,
  addClass: PropTypes.string.isRequired,
  changeSearchTerm: PropTypes.func.isRequired,
};

Header.defaultProps = {
  searchTerm: null,
  logo: null,
};

export default CSSModules(Header, styles, { allowMultiple: true });
