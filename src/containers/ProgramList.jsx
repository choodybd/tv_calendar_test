import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import { API_URL, API_PROGRAMS } from '../config/Api';
import ScheduleOperator from '../components/ScheduleOperator';
import ScheduleObjectsList from './ScheduleObjectsList';
import ScheduleList from '../components/ScheduleList';
import ErrorHandler from '../components/ErrorHandeler';
import { THIS_SHOULD_BE_DATE_NOW } from '../config/Config';
import styles from '../css/style.css';

class ProgramList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schedule: {},
      operatorsVisibility: '',
      loaded: false,
      errorMsg: '',
      markerHeight: 0,
      markerLoaded: false,
      disableClick: '',
      currentDate: THIS_SHOULD_BE_DATE_NOW,
    };
  }

  componentDidMount() {
    this.checkScrolling();
    this.setMarkerHeight(500, true);
    fetch(API_URL + API_PROGRAMS, {
      method: 'GET',
    })
      .then(response => {
        return response.json().then(data => {
          this.setState({
            schedule: data.channels,
          });
          setTimeout(() => {
            this.setState({
              loaded: true,
            });
          }, 1000);
        });
      })
      .catch(error => {
        this.setState({
          errorMsg: error.message,
        });
      });
  }

  componentWillReceiveProps(nextProps) {
    this.setMarkerHeight(1, false);
    this.setState({
      currentDate: nextProps.currentDate,
    });
  }

  setMarkerHeight = (timeout, loading) => {
    setTimeout(() => {
      if (document.querySelector('.p-lis')) {
        const height =
          document.querySelector('.p-lis').clientHeight + 50;
        if (loading) {
          this.setState({
            markerHeight: height,
            markerLoaded: true,
          });
        } else {
          this.setState({
            markerHeight: height,
          });
        }
      }
    }, timeout);
  };

  checkScrolling = () => {
    const element = document.querySelector('.tim');
    const operatorList = document.querySelector('.o-lis');
    element.addEventListener('mousedown', () => {
      element.onmousemove = () => {
        if (this.state.operatorsVisibility === '') {
          this.setState({
            operatorsVisibility: ' hidden',
            disableClick: ' disable',
          });
        }
      };
    });
    element.addEventListener('mouseup', () => {
      element.onmousemove = null;
      operatorList.style.left = `${element.scrollLeft}px`;
      this.setState({
        operatorsVisibility: '',
        disableClick: '',
      });
    });
    element.addEventListener('touchstart', () => {
      this.setState({
        operatorsVisibility: ' hidden',
      });
    });
    element.addEventListener('touchend', () => {
      operatorList.style.left = `${element.scrollLeft}px`;
      this.setState({
        operatorsVisibility: '',
      });
    });
  };

  render() {
    const markerLoaded = this.state.markerLoaded ? ' loaded' : '';
    const schedule = Object.values(this.state.schedule).filter(
      item =>
        item.title
          .toLowerCase()
          .indexOf(this.props.searchTerm.toLowerCase()) !== -1,
    );
    const loaded = this.state.loaded ? ' loaded' : '';
    const scheduleList = schedule.map(value => {
      return value.schedules;
    });
    const scheduleOperator = (value, index) => {
      return (
        <ScheduleObjectsList
          key={index}
          data={value}
          date={this.state.currentDate}
        />
      );
    };
    const operators = (value, index) => {
      const image = value.images.logo;
      const title = value.title;
      return <ScheduleOperator key={index} image={image} title={title} />;
    };
    if (this.state.errorMsg) {
      return <ErrorHandler message={this.state.errorMsg} />;
    }
    return (
      <div>
        <div
          className="ctm"
          styleName={`currentTimeMarker timeline${markerLoaded}`}
          style={{ height: this.state.markerHeight }}
        />
        <div styleName="schedule">
          <div styleName={`preloader${loaded}`} />
          <ScheduleList
            data={schedule.map(operators)}
            c="o-lis"
            name={`o-lis${loaded}${this.state.operatorsVisibility}`}
            list
          />
          <ScheduleList
            data={scheduleList.map(scheduleOperator)}
            c="p-lis"
            name={`p-lis${loaded}${this.state.disableClick}`}
          />
        </div>
      </div>
    );
  }
}

ProgramList.propTypes = {
  searchTerm: PropTypes.string,
  currentDate: PropTypes.string,
};

ProgramList.defaultProps = {
  searchTerm: null,
  currentDate: null,
};

export default CSSModules(ProgramList, styles, { allowMultiple: true });
