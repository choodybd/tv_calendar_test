import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SingleShow from '../components/SingleShow';
import { CALENDAR_WIDTH } from '../config/Config';

class ScheduleObjectsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schedule: props.data,
    };
  }

  render() {
    const operator = (value, index) => {
      const checkDay = new Date(value.start).getDate() < 10 ? `0${new Date(value.start).getDate()}` : new Date(value.start).getDate();
      const checkMonth = new Date(value.start).getMonth() + 1 < 10 ? `0${new Date(value.start).getMonth() + 1}` : new Date(value.start).getMonth() + 1;
      const checkDate = `${new Date(value.start).getFullYear()}-${checkMonth}-${checkDay}`;
      if (checkDate === this.props.date) {
        const showId = value.id;
        const title = value.title;
        const startHour = new Date(value.start).getHours();
        let startMinutes = new Date(value.start).getMinutes();
        const endHour = new Date(value.end).getHours();
        let endMinutes = new Date(value.end).getMinutes();
        const length =
          (endHour * 60 + endMinutes - (startHour * 60 + startMinutes)) *
          CALENDAR_WIDTH;
        if (startMinutes < 10) {
          startMinutes = `0${startMinutes}`;
        }
        if (endMinutes < 10) {
          endMinutes = `0${endMinutes}`;
        }
        const start = `${startHour}:${startMinutes}`;
        const end = `${endHour}:${endMinutes}`;
        return (
          <SingleShow
            key={index}
            title={title}
            start={start}
            end={end}
            showId={showId}
            length={length}
          />
        );
      }
      return null;
    };
    const items = this.state.schedule.map(operator);
    return (
      <ul>
        {items}
      </ul>
    );
  }
}

ScheduleObjectsList.propTypes = {
  data: PropTypes.array.isRequired,
  date: PropTypes.string.isRequired,
};

export default ScheduleObjectsList;
