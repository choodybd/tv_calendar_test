import React, { Component } from 'react';
import BackgroundImage from 'react-background-image-loader';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import Header from './Header';
import logo from '../assets/logo.png';
import ErrorHandler from '../components/ErrorHandeler';
import SeasonButton from '../components/SeasonButton';
import { API_URL } from '../config/Api';
import Image from '../components/Image';
import styles from '../css/style.css';

class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      show: null,
      loaded: false,
      errorMsg: '',
      fade: false,
      season: 0,
    };
  }

  componentDidMount() {
    fetch(API_URL + this.props.location.pathname, {
      method: 'GET',
    })
      .then(response => {
        return response.json().then(data => {
          this.setState({
            show: data,
          });
          setTimeout(() => {
            this.setState({
              loaded: true,
            });
          }, 1000);
          setTimeout(() => {
            this.setState({
              fade: true,
            });
          }, 1500);
        });
      })
      .catch(error => {
        this.setState({
          errorMsg: error.message,
        });
      });
  }

  changeSearchTerm = e => {
    this.setState({
      searchTerm: e.target.value.substr(0, 20),
    });
  };

  showSeason = (e) => {
    this.setState({
      season: e.target.dataset.season,
    });
  };

  render() {
    const loaded = this.state.loaded ? ' loaded' : '';
    const fade = this.state.fade ? ' fade' : '';
    if (this.state.errorMsg) {
      return <ErrorHandler message={this.state.errorMsg} />;
    }
    if (this.state.loaded) {
      const data = this.state.show;
      const image = (data.images && data.images.icon) ? data.images.icon : '';
      const channelImage = (data.channelImages && data.channelImages.logo) ? <Image src={data.channelImages.logo} alt={data.title} /> : '';
      const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      const day = new Date(data.start).getDate();
      const month = new Date(data.start).getMonth();
      const start = new Date(data.start).toLocaleTimeString();
      const end = new Date(data.end).toLocaleTimeString();
      const meta = (value, index) => {
        return <div styleName="singleGenre" key={index}>{value}</div>;
      };
      const person = (value, index) => {
        return <div styleName="singlePerson" key={index}>{value.name}</div>;
      };
      const seasons = (value, index) => {
        const addClass = Number(this.state.season) === index ? ' active' : '';
        return (
          <SeasonButton
            title={value.title}
            clickEvent={this.showSeason}
            season={index}
            key={index}
            addClass={addClass}
          />
        );
      };
      const currentEpisodes = Object.values(this.state.show.series)[this.state.season];
      const episodes = (value, index) => {
        return (
          <div styleName="singleEpisode" key={index}>
            <span styleName="episodeNumber">Episode {index + 1}:</span>
            <span styleName="episodeTitle">{value.title}</span>
            <i styleName="fa fa-play" />
          </div>
        );
      };
      return (
        <div styleName="singleShowPage">
          <Header
            logo={logo}
            changeSearchTerm={this.changeSearchTerm}
            searchTerm={this.state.searchTerm}
            addClass="showPage"
          />
          <div styleName={`showContainer${fade}`}>
            <BackgroundImage styleName="headImage" src={image} placeholder={''}>
              <div styleName="preloader preloader-image" />
            </BackgroundImage>
            <div styleName="wrapper">
              <div styleName="titleContainer">
                <div styleName="col-3 col-logo">
                  {channelImage}
                </div>
                <div styleName="col-9 col-title">
                  <div><span styleName="channelTitle">{data.channelTitle}</span><span styleName="dateTime">{start.substr(0, 5)} - {end.substr(0, 5)} &bull; {day} {monthNames[month]}</span></div>
                  <div styleName="showTitle">
                    <h1>{data.title}</h1>
                  </div>
                  <div styleName="titleFooter">
                    <div><span styleName="metaYear">{data.meta.year}</span><div styleName="metaGenres">{data.meta.genres.map(meta)}</div></div>
                  </div>
                </div>
              </div>
              <div styleName="mainContainer">
                {data.description}
              </div>
              <div styleName="personContainer">
                <div styleName="personTitle">Cast: </div>{data.meta.cast.map(person)}
              </div>
              <div styleName="personContainer">
                <div styleName="personTitle">Creators: </div>{data.meta.creators.map(person)}
              </div>
              <div styleName="seasonsNavigation">
                {data.series.map(seasons)}
              </div>
              <div styleName="seasonsContainer">
                {currentEpisodes.episodes.map(episodes)}
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div styleName="singleShowPage">
        <Header
          logo={logo}
          changeSearchTerm={this.changeSearchTerm}
          searchTerm={this.state.searchTerm}
          addClass="showPage"
        />
        <div styleName={`preloader${loaded}`} />
      </div>
    );
  }
}

Show.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }),
};

Show.defaultProps = {
  location: {},
};

export default CSSModules(Show, styles, { allowMultiple: true });
