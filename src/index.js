/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './containers/App';
import Show from './containers/Show';
import './css/style.css';

ReactDOM.render(
  <BrowserRouter>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/program/:id" component={Show} />
    </div>
  </BrowserRouter>,
  document.getElementById('root'),
);
/* eslint-enable react/jsx-filename-extension */
